﻿using Eco.Core.Plugins.Interfaces;
using EcoColorLib;
using MoneyStats.Config;
using MoneyStats.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats
{
    public class MoneyStats : IModKitPlugin, IServerPlugin
    {
        public static String prefix = "MoneyStats: ";
        public static String prefixColored = ChatFormat.Green.Value + ChatFormat.Bold.Value + "MoneyStats: " + ChatFormat.Clear.Value;

        public MoneyStats()
        {
            Console.WriteLine(prefix + " initialized !");

            GeneralConfig.obj = new GeneralConfig("MoneyStat", "generalConfig");
            if (GeneralConfig.obj.exist())
            {
                GeneralConfig.obj = GeneralConfig.obj.reload<GeneralConfig>();
            }
            else
            {
                GeneralConfig.obj.save();
            }


            RapportsListe.obj = new RapportsListe("MoneyStat", "rapports");
            if (RapportsListe.obj.exist())
            {
                RapportsListe.obj = RapportsListe.obj.reload<RapportsListe>();
            }
            else
            {
                RapportsListe.obj.save();
            }


            Rapport last = Rapport.getLastRapport();
            if(last!=null)
            {
                String lastdate = last.generateDate.ToString("dd/MM/yyyy");
                Console.WriteLine(prefix + "Last rapport: "+ lastdate);
                Console.WriteLine(prefix + "Rapports loaded: " + RapportsListe.obj.rapports.Count);
            }
           

            System.Threading.Thread objw = new System.Threading.Thread(() => RapportWatcher.checkRapportCreation());
            objw.Start();

        }

        public string GetStatus()
        {
            return "";
        }
    }
}
