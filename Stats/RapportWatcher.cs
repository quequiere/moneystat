﻿using Eco.Gameplay.Economy;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared.Services;
using EcoColorLib;
using MoneyStats.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoneyStats.Stats
{
   public class RapportWatcher
    {

        public static void checkRapportCreation()
        {
            while(true)
            {
                if(needCreateRepport())
                {
                    //attente du boot serveur ou delais general de 60 secondes
                    Thread.Sleep(60 *1000);
                    Rapport rapport = Rapport.createRapport();
                    if(rapport==null)
                    {
                        Console.WriteLine(MoneyStats.prefix + "Rapport thread canceled cause can't generate new rapport !");
                        return;
                    }
                    RapportsListe.obj.rapports.Add(rapport);
                    RapportsListe.obj.save();
                    Console.WriteLine(MoneyStats.prefix + "A new money rapport has been created !");
                    taxe(rapport);
                }

                Thread.Sleep(60 * 60 * 1000);
            }
        }

        public static void taxe(Rapport newRapport)
        {

            float prelevement = 0;

            foreach (UserData u in newRapport.getActivePlayer())
            {
                if (u.money > newRapport.moneyNeededToBeTaxeFromISF())
                {
                    float taxe = (u.money * GeneralConfig.obj.taxeISFPercent);
                    prelevement+=tryToGetMoney(taxe,u.name);
                }
            }


            foreach (UserData u in newRapport.getAfkPlayers())
            {
                float taxe = (u.money * GeneralConfig.obj.afkTaxePercent);
                prelevement += tryToGetMoney(taxe, u.name);
            }

            List<UserData> beneficiaire = new List<UserData>();

            foreach (UserData u in newRapport.getActivePlayer())
            {
                double hourPlayed = (u.playedTime) / 60 / 60;
                if (u.money <= newRapport.moneyNeededToBeRewardFromISF() && hourPlayed >= GeneralConfig.obj.hourNeedToGetBeneficiaire)
                {
                    beneficiaire.Add(u);
                }
            }

            float toGive = prelevement / beneficiaire.Count;

            foreach (UserData u in beneficiaire)
            {
                tryToGive(toGive, u.name);
            }


            ChatManager.ServerMessageToAllAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.Blue.Value + $"Nouveau jour ! Les taxes ont été prélevées, {prelevement} {GeneralConfig.obj.moneyname} pris et redistribués entre {beneficiaire.Count} bénéficiaires soit {toGive} {GeneralConfig.obj.moneyname} par personnes.", false, DefaultChatTags.General);
        }

        public static void tryToGive(float montant, String name)
        {
            String moneyName = GeneralConfig.obj.moneyname;
            Currency currency = EconomyManager.Currency.GetCurrency(moneyName);

            if (currency == null)
            {
                Console.WriteLine(MoneyStats.prefix + "Impossible de trouver la monnaie " + moneyName);
                return;
            }

            Account target = currency.GetAccount(name);
            target.SetVal(target.Val + montant);
        }


        public static float tryToGetMoney(float tax,String name)
        {
            String moneyName = GeneralConfig.obj.moneyname;
            Currency currency = EconomyManager.Currency.GetCurrency(moneyName);

            if (currency == null)
            {
                Console.WriteLine(MoneyStats.prefix + "Impossible de trouver la monnaie " + moneyName);
                return 0;
            }

            Account target = currency.GetAccount(name);
            if(target.Val<=1)
            {
                return 0;
            }
            else if(target.Val<tax)
            {
                float temp = target.Val;
                target.SetVal(0);
                return temp;
            }
            else
            {
                target.SetVal(target.Val-tax);
                return tax;
            }
        }

        public static Boolean needCreateRepport()
        {
            Rapport betterRapport = Rapport.getLastRapport();

            if(betterRapport==null)
            {
                Console.WriteLine(MoneyStats.prefix + "need to create repport cause no repport in configs !");
                return true;
            }


            DateTime now = DateTime.Now;
            String currentDay = now.ToString("dd/MM/yyyy");
            String lastDay = betterRapport.generateDate.ToString("dd/MM/yyyy");

            if(!currentDay.Equals(lastDay))
            {
                Console.WriteLine(MoneyStats.prefix + "Day changed ! Need to create a new repport : "+ currentDay +" VS "+ lastDay);
                return true;
            }

            return false;
        }
    }
}
