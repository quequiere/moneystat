﻿using Eco.Gameplay.Objects;
using Eco.Shared;
using Eco.Shared.Math;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Stats
{
    class TopSort : IComparer<Evolution>
    {
        bool hight;
        public TopSort(bool hight)
        {
            this.hight = hight;
        }

        public int Compare(Evolution o1, Evolution o2)
        {
            if (hight)
            {
                return o1.getPersonnalGain().CompareTo(o2.getPersonnalGain());
            }
            else
            {
                return o1.getPersonnalGain().CompareTo(o2.getPersonnalGain()) * -1;
            }
        }



       
    }
}
