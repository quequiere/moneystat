﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Stats
{
    public class Evolution
    {
        public String username;
        public UserData recente;
        public UserData old;

        public Evolution(String username,UserData recente,UserData old)
        {
            this.username = username;
            this.recente = recente;
            this.old = old;
        }

        public float getPersonnalGain()
        {
           return recente.money - old.money;
        }
    }
}
