﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Stats
{

    public class UserData
    {
        [JsonProperty]
        public String name;
        [JsonProperty]
        public float money;
        [JsonProperty]
        public double secondsSinceLogout;
        [JsonProperty]
        public double playedTime;

        public UserData(String name,float money,double afkTime,double playedTime)
        {
            this.name = name;
            this.money = money;
            this.secondsSinceLogout = afkTime;
            this.playedTime = playedTime;
        }

    }
}
