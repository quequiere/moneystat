﻿using Eco.Gameplay.Economy;
using Eco.Gameplay.Players;
using Eco.Simulation.Time;
using MoneyStats.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Stats
{
    public class Rapport
    {
        [JsonProperty]
        public DateTime generateDate = DateTime.Now;
        [JsonProperty]
        public List<UserData> users;

        public Rapport(List<UserData> data)
        {
            this.users = data;
        }


        public static Rapport createRapport()
        {
            List<UserData> users = new List<UserData>();
            foreach (User user in UserManager.Users)
            {
                double secondsSinceOffline = WorldTime.Seconds - user.LogoutTime;
                float money = 0;

                String moneyName = GeneralConfig.obj.moneyname;
                Currency currency = EconomyManager.Currency.GetCurrency(moneyName);

                if (currency == null)
                {
                    Console.WriteLine(MoneyStats.prefix + "We can't fin this money name: " + moneyName);
                    return null;
                }

                Account account = currency.GetAccount(user.Name);
                money = account.Val;

                users.Add(new UserData(user.Name, money, secondsSinceOffline,user.TotalPlayTime));

            }

            return new Rapport(users);
        }


        public static Rapport getRapportByDate(String date)
        {
            foreach (Rapport r in RapportsListe.obj.rapports)
            {
                if (date.Equals(r.generateDate.ToString("dd/MM/yyyy")))
                {
                    return r;
                }
            }
            return null;
        }

        public static Rapport getLastRapport()
        {
            Rapport betterRapport = null;
            foreach (Rapport rapport in RapportsListe.obj.rapports)
            {
                if (betterRapport == null)
                    betterRapport = rapport;
                else
                {
                    int result = DateTime.Compare(betterRapport.generateDate, rapport.generateDate);
                    if (result == -1)
                    {
                        betterRapport = rapport;
                    }
                }

            }

            return betterRapport;
        }


        public List<UserData> getActivePlayer()
        {
            List<UserData> active = new List<UserData>();

            foreach (UserData u in this.users)
            {
                if (u.secondsSinceLogout <= GeneralConfig.obj.secondsToBeAfk)
                {
                    active.Add(u);
                }
            }

            return active;
        }

        public List<UserData> getAfkPlayers()
        {
            List<UserData> afks = new List<UserData>();

            foreach (UserData u in this.users)
            {
                if (u.secondsSinceLogout > GeneralConfig.obj.secondsToBeAfk)
                {
                    afks.Add(u);
                }
            }

            return afks;
        }

        public float getAverageMoneyPerActivePlayer()
        {
            float totalMoney = 0;
            int count = 0;
            foreach (UserData u in this.getActivePlayer())
            {
                count++;
                totalMoney += u.money;
            }

            float moyenne = totalMoney / count;
            return moyenne;
        }

        public float moneyNeededToBeTaxeFromISF()
        {
            return this.getAverageMoneyPerActivePlayer() * GeneralConfig.obj.pallierHautISF;
        }

        public float moneyNeededToBeRewardFromISF()
        {
            return this.getAverageMoneyPerActivePlayer() * GeneralConfig.obj.pallierBasISF;
        }


        public float getTotalEcoISFToTake()
        {
            float total = 0;
            foreach (UserData u in this.getActivePlayer())
            {
                if (u.money > moneyNeededToBeTaxeFromISF())
                {
                    
                    float taxe = (u.money * GeneralConfig.obj.taxeISFPercent);
                    total += taxe;
                }
            }
            return total;
        }


        public float getTotalEcoAFKToTake()
        {
            float total = 0;
            foreach (UserData u in this.getAfkPlayers())
            {
                float taxe = (u.money * GeneralConfig.obj.afkTaxePercent);
                total += taxe;
                
            }
            return total;
        }

        public int getISFBeneficiaireCount()
        {
            int userCount = 0;
            foreach (UserData u in this.getActivePlayer())
            {
                double hourPlayed = (u.playedTime) / 60 / 60;
                if (u.money <= moneyNeededToBeRewardFromISF() && hourPlayed >= GeneralConfig.obj.hourNeedToGetBeneficiaire )
                {
                    userCount++;
                }

            }
            return userCount;
        }


        public float getISFToDistribute()
        {
            return (float) (getTotalEcoISFToTake()+ getTotalEcoAFKToTake()) / getISFBeneficiaireCount();
        }


        public float getTotalMoneyCirculation()
        {
            float money = 0;
            foreach (UserData u in this.getActivePlayer())
            {
                money += u.money;
            }
            return money;
        }

        public float getTotalMoney()
        {
            float money = 0;
            foreach (UserData u in this.users)
            {
                money += u.money;
            }
            return money;
        }

        public UserData getUserDataByName(String name)
        {
            foreach(UserData u in this.users)
            {
                if (u.name.Equals(name))
                    return u;
            }

            return null;
        }

        //-------------------------------- EVOLUTION STUFF

        public float getInflationTotalePercent(Rapport old)
        {
            float inflation = this.getTotalMoneyCirculation() *100 / old.getTotalMoneyCirculation();
            return inflation-100;
        }

        public float getInflationTotale(Rapport old)
        {
            float inflation = this.getTotalMoneyCirculation() - old.getTotalMoneyCirculation();
            return inflation;
        }


        //---------------------

        public float getEvolutionForPlayerPercent(Rapport old,String name)
        {
            float evolution = this.getUserDataByName(name).money*100/ old.getUserDataByName(name).money;
            return evolution-100;
        }

        public float getEvolutionForPlayer(Rapport old, String name)
        {
            float evolution = this.getUserDataByName(name).money - old.getUserDataByName(name).money;
            return evolution;
        }

        //---------------------

        public float getEvolutionAverageMoneyPercent(Rapport old)
        {
            float evolution = this.getAverageMoneyPerActivePlayer() * 100 / old.getAverageMoneyPerActivePlayer();
            return evolution - 100;
        }

        public float getEvolutionAverageMoney(Rapport old)
        {
            float evolution = this.getAverageMoneyPerActivePlayer() - old.getAverageMoneyPerActivePlayer();
            return evolution;
        }

    }
}
