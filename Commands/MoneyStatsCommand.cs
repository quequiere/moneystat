﻿using Eco.Gameplay;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Players;
using Eco.Gameplay.Property;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared.Services;
using Eco.Simulation.Time;
using EcoColorLib;
using MoneyStats.Config;
using MoneyStats.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Commands
{
    public enum SubCommandsUser
    {
        rapport,
        rapportHistory,
        deposite,
        evolution,
        expel,
    }

    public class MoneyStatsCommand : IChatCommandHandler
    {
        // /db link pseudo
        [ChatCommand("moneystat", "Money stat base command", ChatAuthorizationLevel.User)]
        public static void MoneyStatsUserCommand(User user, String argsString = "")
        {
            String[] args = argsString.Split(' ');


            if (args.Length <= 0 || args[0].Equals(""))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored+"Liste des sous commandes:"));

                foreach (SubCommandsUser sub in Enum.GetValues(typeof(SubCommandsUser)))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + "/moneystat " + ChatFormat.Red.Value + sub.ToString()));
                }
                return;
            }
            else
            {
                SubCommandsUser sub;

                if (!SubCommandsUser.TryParse(args[0], out sub))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Aucune sous commande de type {args[0]}"));
                }
                else
                {

                    if (sub.Equals(SubCommandsUser.rapportHistory))
                    {
                        if (args.Length <= 1 || args[1].Equals(""))
                        {
                            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Merci de préciser la date du rapport ainsi: /moneystat rapport 06/04/2018"));
                        }
                        else
                        {
                            Rapport r = Rapport.getRapportByDate(args[1]);
                            if (r == null)
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Impossible de trouver le rapport du: {args[1]}."));
                            }
                            else
                            {
                                displayRapport(r, user);
                            }
                        }

                    }
                    else if (sub.Equals(SubCommandsUser.rapport))
                    {
                        displayRapport(Rapport.getLastRapport(), user);
                    }
                    else if (sub.Equals(SubCommandsUser.deposite))
                    {
                        if (args.Length <= 1)
                        {
                            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Merci de préciser le montant à donner à la ville. /moneystat deposite 12.8"));
                        }
                        else
                        {
                            float montant;
                            if (!float.TryParse(args[1], out montant))
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Montant invalide: {args[1]}"));
                            }
                            else
                            {
                                deposite(user, montant);
                            }
                        }
                    }
                    else if (sub.Equals(SubCommandsUser.evolution))
                    {
                        if (args.Length <= 1)
                        {
                            displayEvolution(user, 7);
                        }
                        else
                        {
                            int jour;
                            if (!int.TryParse(args[1], out jour))
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Nombre de jour invalide: {args[1]}"));
                            }
                            else
                            {
                                displayEvolution(user, jour);
                            }
                        }
                    }
                    else if (sub.Equals(SubCommandsUser.expel))
                    {
                        if (Legislation.Government.LeaderUser != user)
                        {
                            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Vous devez être leader pour faire cette commande."));
                            return;
                        }
                        else
                        {
                            String owner = PropertyManager.Owner(user.Player.Position.XZi);

                            if(owner==null || owner.Equals(" "))
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Cet endroit n'est pas claim."));
                                return;
                            }

                            User target = UserManager.FindUserByName(owner);

                            if (target == null)
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"We can't find user: "+owner));
                                return;
                            }

                            double offlineTimeSeconds = WorldTime.Seconds - target.LogoutTime; 

                            if (offlineTimeSeconds > GeneralConfig.obj.secondsToBeAfk)
                            {
                                Tuple<string, bool> result = PropertyManager.TryUnclaimProperty(user.Player.Position.XZi);
                                if (result.Item2)
                                    user.Player.SendTemporaryMessageLoc(result.Item1);
                                else
                                    user.Player.SendTemporaryErrorLoc(result.Item1);
                            }
                            else
                            {
                                double reste = GeneralConfig.obj.secondsToBeAfk - offlineTimeSeconds;
                                reste = Math.Round(reste);
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + $"Il reste "+ reste+ " secondes à ce joueur avant de pouvoir être expulsé!"));
                                return;
                            }



                        }
                    }
                    else
                    {
                        user.Player.SendTemporaryMessage(FormattableStringFactory.Create(MoneyStats.prefixColored + "Erreur de commande inconnue"));
                    }
                }

            }
        }

        public static void displayEvolution(User u, int day)
        {
            DateTime targetTime = DateTime.Now.AddDays(-day);
            Rapport r = Rapport.getRapportByDate(targetTime.ToString("dd/MM/yyyy"));

            if(r==null)
            {
                ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Impossible de trouver le rapport en date du : {targetTime.ToString("dd/MM/yyyy")} vous pouvez utilisez aussi la commande de cette manière /moneystat evolution nombredejour", u, false, DefaultChatTags.General);
                return;
            }

            Rapport futur = Rapport.createRapport();

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.UnderLine.Value + "Rapport du " + r.generateDate.ToString("dd/MM/yyyy") + " VS maintenant.", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);


            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Blue.Value + $"Inflation: {futur.getInflationTotale(r)} {GeneralConfig.obj.moneyname} {ChatFormat.Gray.Value}( {futur.getInflationTotalePercent(r).ToString("0.00")}% )", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Green.Value + $"Votre évolution: {futur.getEvolutionForPlayer(r,u.Name)} {GeneralConfig.obj.moneyname} {ChatFormat.Gray.Value}( {futur.getEvolutionForPlayerPercent(r,u.Name).ToString("0.00")}% )", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Yellow.Value + $"Evolution moyenne des joueurs actifs: {futur.getEvolutionAverageMoney(r)} {GeneralConfig.obj.moneyname} {ChatFormat.Gray.Value}( {futur.getEvolutionAverageMoney(r).ToString("0.00")}% )", u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);

            displayTop(u,futur,r);

        }

        public static void deposite(User u, float ammount)
        {
            String moneyName = GeneralConfig.obj.moneyname;
            Currency currency = EconomyManager.Currency.GetCurrency(moneyName);

            if (currency == null)
            {
                ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Impossible de trouver la monnaie nommée: {moneyName}", u, false, DefaultChatTags.General);
                return;
            }
            Account target = currency.GetAccount(AccountNames.TreasuryName);

            if (target==null)
            {
                ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Il n'y a pas de coffre dans la capitale pour la monaie {moneyName}", u, false, DefaultChatTags.General);
                return;
            }

            Account sender = currency.GetAccount(u.Name);

            if (sender.Val<ammount)
            {
                ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Vous n'avez pas assez d'argent pour faire cela.", u, false, DefaultChatTags.General);
                return;
            }
            sender.SetVal(sender.Val - ammount);
            target.SetVal(target.Val + ammount);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Green.Value + $"Vous avez effectué une donation de {ammount} {moneyName}", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToAllAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value+ ChatFormat.Blue.Value + $"{u.Name} a effectué une généreuse donnation de {ammount} {moneyName} aux coffres de la capitale.", false, DefaultChatTags.General);

        }

        public static void displayRapport(Rapport rapport, User u)
        {
            String m = GeneralConfig.obj.moneyname;
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.UnderLine.Value + "Rapport du " + rapport.generateDate.ToString("dd/MM/yyyy") +" à minuit.", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Gray.Value + "- Total d'AFK: "+rapport.getAfkPlayers().Count(), u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Yellow.Value + "- Total d'Actif: " + rapport.getActivePlayer().Count(), u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized( " ", u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Blue.Value + "- Argent moyen des joueurs actifs: " + Math.Round(rapport.getAverageMoneyPerActivePlayer()) + " "+m, u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Yellow.Value + "- Total d'argent en circulation " + Math.Round(rapport.getTotalMoneyCirculation()) + " " + m, u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Gray.Value + "- Total d'argent " + Math.Round(rapport.getTotalMoney()) + " " + m, u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);

            Rapport futur = Rapport.createRapport();

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.UnderLine.Value + "ISF prévisionnel applicable demain:", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Blue.Value + $"Les joueurs ayant joué au moins {GeneralConfig.obj.hourNeedToGetBeneficiaire} heures et ayant moins de {Math.Floor(futur.moneyNeededToBeRewardFromISF())} {m} toucheront {futur.getISFToDistribute()} {m}", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Blue.Value + $"Nombre de bénéficiaire: {futur.getISFBeneficiaireCount()}", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Les joueurs ayant plus de {Math.Floor(futur.moneyNeededToBeTaxeFromISF())} {m} seront taxé à hauteur de {GeneralConfig.obj.taxeISFPercent*100} %", u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized( " ", u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.UnderLine.Value + "Taxe des AFK:", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Les joueurs innactifs depuis {GeneralConfig.getJourToAfk()} jours seront taxé à hauteur de {GeneralConfig.obj.afkTaxePercent*100}%. Cet argent est ajouté à l'ISF.", u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Total ramassé par l'ISF: {Math.Round(rapport.getTotalEcoISFToTake())} {m}", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"Total ramassé par les taxes AFK: {Math.Round(rapport.getTotalEcoAFKToTake())} {m}", u, false, DefaultChatTags.General);
        }


        public static void displayTop(User u,Rapport r, Rapport old)
        {
            List<Evolution> evolutions = new List<Evolution>();
            foreach(UserData dataNew in r.users)
            {
                UserData oldData = old.getUserDataByName(dataNew.name);

                if(oldData!=null)
                {
                    evolutions.Add(new Evolution(dataNew.name, dataNew, oldData));
                }
                
            }
            evolutions.Sort(new TopSort(false));

            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.UnderLine.Value + "Top 10 des évolutions positives: ", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);
            int x = 1;
            foreach(Evolution e in evolutions)
            {
                ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Blue.Value + $"{e.getPersonnalGain()} {GeneralConfig.obj.moneyname} ==> {ChatFormat.Bold.Value+e.username}", u, false, DefaultChatTags.General);
                x++;
                if (x > 10)
                    break;
            }

            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Bold.Value + ChatFormat.UnderLine.Value + "Top 10 des évolutions négatives: ", u, false, DefaultChatTags.General);
            ChatManager.ServerMessageToPlayerAlreadyLocalized(" ", u, false, DefaultChatTags.General);

            evolutions.Sort(new TopSort(true));
             x = 1;
            foreach (Evolution e in evolutions)
            {
                ChatManager.ServerMessageToPlayerAlreadyLocalized(MoneyStats.prefixColored + ChatFormat.Red.Value + $"{e.getPersonnalGain()} {GeneralConfig.obj.moneyname} ==> {ChatFormat.Bold.Value + e.username}", u, false, DefaultChatTags.General);
                x++;
                if (x > 10)
                    break;
            }

        }
    }
}
