﻿using JsonConfigSaver;
using MoneyStats.Stats;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Config
{
    public class RapportsListe : JsonEcoConfig
    {
        public static RapportsListe obj;

        [JsonProperty]
        public List<Rapport> rapports = new List<Rapport>();

        public RapportsListe(string pluginName, string name) : base(pluginName, name)
        {

        }



    }
}
