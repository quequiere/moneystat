﻿using JsonConfigSaver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyStats.Config
{
    public class GeneralConfig : JsonEcoConfig
    {
        public static GeneralConfig obj;


        [JsonProperty]
        public String moneyname ="Eco";

        [JsonProperty]
        public int secondsToBeAfk = 60 * 60 * 24 * 4;

        [JsonProperty]
        public float afkTaxePercent =0.02f;

        [JsonProperty]
        public float pallierBasISF = 0.15f;

        [JsonProperty]
        public float pallierHautISF = 5f;

        [JsonProperty]
        public float taxeISFPercent = 0.01f;

        [JsonProperty]
        public int hourNeedToGetBeneficiaire = 10;



        public GeneralConfig(string pluginName, string name) : base(pluginName, name)
        {

        }

        public static int getJourToAfk()
        {
            return GeneralConfig.obj.secondsToBeAfk / 60/60/24;
        }
    }
}
